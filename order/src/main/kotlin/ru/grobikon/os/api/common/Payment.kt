package ru.grobikon.os.api.common

data class Payment(
    var id: Int? = null,
    var paymentStatus: String? = null,
    var transactionId: String? = null,
    var orderId: Int? = null,
    var amount: Double? = null
)
