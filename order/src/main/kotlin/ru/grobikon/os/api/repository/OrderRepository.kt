package ru.grobikon.os.api.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.grobikon.os.api.entity.Order

interface OrderRepository: JpaRepository<Order, Int> {

}