package ru.grobikon.os.api.entity

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "Order_TB")
data class Order(
    @Id
    var id: Int,
    var name: String,
    var qty: Int,
    var price: Double
)
