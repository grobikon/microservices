package ru.grobikon.os.api.common

import ru.grobikon.os.api.entity.Order

data class TransactionRequest(
    var order: Order? = null,
    var payment: Payment? = null
)