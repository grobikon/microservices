package ru.grobikon.os.api.common

import ru.grobikon.os.api.entity.Order

data class TransactionResponse(
    var order: Order? = null,
    var amount: Double? = null,
    var transactionId: String? = null,
    var message: String? = null
)