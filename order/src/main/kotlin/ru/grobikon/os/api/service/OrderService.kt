package ru.grobikon.os.api.service

import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import ru.grobikon.os.api.common.Payment
import ru.grobikon.os.api.common.TransactionRequest
import ru.grobikon.os.api.common.TransactionResponse
import ru.grobikon.os.api.repository.OrderRepository

@Service
class OrderService(
    val repository: OrderRepository,
    val template: RestTemplate
) {

    fun saveOrder(request: TransactionRequest): TransactionResponse? {
        val order = request.order
        //Создаём объект оплаты
        val payment = request.payment
        if (order != null && payment != null) {
            payment.orderId = order.id
            payment.amount = order.price
            //отправляем Rest запросы в другой микросервис api payment-api
            //rest call
            val paymentResponse = template.postForObject("http://PAYMENT-SERVICE/payment/doPayment", payment, Payment::class.java)
            repository.save(order)

            val message = when(paymentResponse?.paymentStatus == "success") {
                true -> "платеж обработан успешно и размещен"
                false -> "сбой в API оплаты, заказ добавлен на карту"
            }

            return TransactionResponse(
                order = order,
                amount = paymentResponse?.amount,
                transactionId = paymentResponse?.transactionId,
                message = message)
        }
        return null
    }
}