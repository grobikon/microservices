package ru.grobikon.os.api.controller

import org.springframework.web.bind.annotation.*
import ru.grobikon.os.api.common.TransactionRequest
import ru.grobikon.os.api.common.TransactionResponse
import ru.grobikon.os.api.service.OrderService

@RestController
@RequestMapping("/order")
class OrderController(
    val orderService: OrderService
) {

    @PostMapping("/bookOrder")
    fun bookOrder(@RequestBody request: TransactionRequest): TransactionResponse? {
        //do a rest call to payment and pass the order id
        return orderService.saveOrder(request)
    }
}