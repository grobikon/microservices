package ru.gobikon.serviceregistry

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

/**
 * @EnableEurekaServer говорит что это приложение будет сервером
 */
@SpringBootApplication
@EnableEurekaServer
class MicroservicesApplication

fun main(args: Array<String>) {
	runApplication<MicroservicesApplication>(*args)
}
