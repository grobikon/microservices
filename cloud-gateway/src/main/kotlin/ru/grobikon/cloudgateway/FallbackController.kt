package ru.grobikon.cloudgateway

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class FallbackController {

    @RequestMapping("/orderFallback")
    fun orderServiceFallBack(): Mono<String> {
        return Mono.just("Запрос к Order Service занимает слишком много времени или недоступен. Пожалуйста, попробуйте позже.")
    }

    @RequestMapping("/paymentFallback")
    fun paymentServiceFallBack(): Mono<String> {
        return Mono.just("Запрос к Payment Service занимает слишком много времени или недоступен. Пожалуйста, попробуйте позже.")
    }
}