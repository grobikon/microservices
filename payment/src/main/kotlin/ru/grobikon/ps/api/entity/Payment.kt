package ru.grobikon.ps.api.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "Payment_TB")
data class Payment(
    @Id
    @GeneratedValue
    var id: Int? = null,
    var paymentStatus: String? = null,
    var transactionId: String? = null,
    var orderId: Int? = null,
    val amount: Double? = null
)
