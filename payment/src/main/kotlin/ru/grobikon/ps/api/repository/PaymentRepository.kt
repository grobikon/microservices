package ru.grobikon.ps.api.repository

import org.springframework.data.jpa.repository.JpaRepository
import ru.grobikon.ps.api.entity.Payment

interface PaymentRepository: JpaRepository<Payment, Int> {
    fun findByOrderId(orderId: Int): Payment
}