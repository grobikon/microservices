package ru.grobikon.ps.api.controller

import org.springframework.web.bind.annotation.*
import ru.grobikon.ps.api.entity.Payment
import ru.grobikon.ps.api.service.PaymentService

@RestController
@RequestMapping("/payment")
class PaymentController(
    val paymentService: PaymentService
) {

    @RequestMapping("/doPayment")
    fun doPayment(@RequestBody payment: Payment): Payment {
        return paymentService.doPayment(payment)
    }

    @GetMapping("/{orderId}")
    fun findPaymentHistoryByOrderId(@PathVariable orderId: Int): Payment {
        return paymentService.findPaymentHistoryByOrderId(orderId)
    }
}