package ru.grobikon.ps.api.service

import org.springframework.stereotype.Service
import ru.grobikon.ps.api.entity.Payment
import ru.grobikon.ps.api.repository.PaymentRepository
import java.util.*

@Service
class PaymentService(
    var paymentRepository: PaymentRepository
) {

    fun doPayment(payment: Payment): Payment {
        payment.paymentStatus = paymentProcessing()
        payment.transactionId = UUID.randomUUID().toString()
        return paymentRepository.save(payment)
    }

    /**
     * Обработка платежей, рандомно определяем статус платежа
     */
    fun paymentProcessing(): String{
        //оплата должна вызываться 3й стороной (paypal, paytm...)
        return if (Random().nextBoolean()) "success" else "false"
    }

    /**
     * Получаем информацию о заказе по его ID заказа
     * @param orderId - id заказа, который будем искать
     */
    fun findPaymentHistoryByOrderId(orderId: Int): Payment {
        return paymentRepository.findByOrderId(orderId)
    }
}